## News application Project
The project is written with Swift 5, support above iOS 11.0
Just git clone the project, or download as a package.
Open with the latest xcode ( > 11.0)

 - For UI testing, I use wiremock as a local server, supply the mock data for testing purpose, the wiremock projecct is uploaded on https://gitlab.com/xiao/wiremockfornewsapp, please downlod the package for UI testing purpose
 - To run locally, go open a terminal and go to the root directory of this project
Run the following command:
```
$ java -jar wiremock-1.57-standalone.jar
```

 - Wiremock is exposed on port 8080 and you should be able to do a GET http://localhost:8080/__admin/ to see it's current configuration details

- From XCode
When you open the project select scheme newsappUITests and Select Test to run

----
## API
  - News API :http://localhost:8080/1/coding_test/13ZZQX/full
----
## The explicit requirements are:

- As a user(of the application) I can see the list of news, which are sorted by the timestamp, latest will be on the top.
- As a user I can see the section head view, the unqiue section list which is get from the category of each asset. the value is from the  first level of "sectionPath".
- As a user I can see totalnews/4 news in the top section (there is no specific requirement which news should be the top news, i assumption this is the rule for top news).
- As a user I can tap on each section, and page will be refreshed to show the news belong to that section.
- As a user I can see the image, title, abstract, byLine user of each news.
- As a user I can pull down the news list to refresh the page, then page will be reloaded to show the latest top section news.
- As a user When there is no internet, when I try to refresh the page. I will see the popover view with "no interent" alert view
----
##  Architecutre

#### Model
 - News.swift
 	- News
 	- Asset
 	- Image
 	- Category
 	- Author
 	- Company
 	- Source
 	- Overrides
 	- Brand
 	- RelatedAsset
 	- Post

#### ViewModel
 - NewsViewModel.swift
	 - fetch news list
	 - sort news by timestamp
	 - create section list from category
	 - group news by section

#### View
 - NewsViewController
	 - display the list of section
	 - display the news
	 - pull to refresh the page
	 - error handling
----
## ThirdParty Frameworks
 #### target
    - PromiseKit: Promise used in server call
    - RxSwift: used to build MVVM framework
    - RxCocoa
    - ReachabilitySwift: Inetnet checking
    - Kingfisher: Image load
    - NVActivityIndicatorView

  #### Unit test
    - OHHTTPStubs/Swift
    - Nimble
    - RxTest
    - RxBlocking
