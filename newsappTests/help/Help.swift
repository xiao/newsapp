//
//  Help.swift
//  newsappTests
//
//  Created by Jin on 23/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//

import Foundation


struct TestingHelpTool {
    
    static func readJSONSample(fileName:String) -> [String : Any]? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                  if let jsonResult = jsonResult as? [String : Any]{
                        return jsonResult
                  }
              } catch {
                  print("faild to read the json")
                  return nil
              }
        }
        return nil
    }
}
