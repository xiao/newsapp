@testable import newsapp

import XCTest
import PromiseKit
import OHHTTPStubs
import Nimble

class ConnectionManagerTest: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testReadNewsSuccessfully() {
        _ = stub(condition: isPath("/1/coding_test/13ZZQX/full"), response: { _ in
            let jsonObject = TestingHelpTool.readJSONSample(fileName: "news") ?? ["":""]
            return OHHTTPStubsResponse(jsonObject: jsonObject, statusCode: 200, headers: nil)
        })
        
        let service = ConnectionManager()
        waitUntil(timeout: 60) { done in
            firstly {
                return service.getAllNews()
            }.get { news in
                expect(news).toNot(beNil())
                done()
            }.catch { error in
                fail("An error was returned \(error)")
            }
        }
    }
    
    func testFailToReadNewsWith500Error() {
        _ = stub(condition:isPath("/1/coding_test/13ZZQX/full")) { _ in
            let response = OHHTTPStubsResponse()
            response.statusCode = 500
            return response
        }
        
        let service = ConnectionManager()
        waitUntil(timeout: 60) { done in
            firstly {
                return service.getAllNews()
            }.get { news in
                fail("500 error should not be here")
            }.catch { error in
                if let localError = error as? LocalError {
                    expect(localError) == .ServerError
                    done()
                } else {
                    fail("not the error type as we expected")
                }
            }
        }
    }
}
