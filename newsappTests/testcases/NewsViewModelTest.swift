//
//  NewsViewModelTest.swift
//  newsappTests
//
//  Created by Jin on 25/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//
@testable import newsapp
import XCTest
import RxSwift
import PromiseKit
import OHHTTPStubs
import Nimble
import RxTest
import RxBlocking

class NewsViewModelTest: XCTestCase {
   // var viewModel: NewsViewModel!
    var disposeBag: DisposeBag!
   // var scheduler: TestScheduler!


    override func setUp() {
        disposeBag = DisposeBag()
    }

    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testViewModel() {
        let scheduler = TestScheduler(initialClock: 0, resolution: 0.01)
        let viewModel = NewsViewModel(scheduler: scheduler, server: MockConnectionManager())
        viewModel.outputs.updateDashboadHeadersSignal.subscribe(onNext: { info in
            XCTAssertNotNil(info.headers)
            XCTAssertNotNil(info.result)
        }, onError: { error in
            print("error")
        }, onCompleted: {
            print("completed")
        }) {
            print("disposed")
        }.disposed(by: disposeBag)
        scheduler.createColdObservable([.next(10,())]).bind(to: viewModel.refreshDashboardProperty).disposed(by: disposeBag)

        scheduler.start()
    }
}


struct MockConnectionManager: ConnectionManagerProtocol {
    func getAllNews() -> Promise<News> {
        return Promise { seal in
            
            let emptyNews = News(id: 0, categories: [NewsCategory](), authors: [Author](), url: "", lastModified: 0, onTime: 0, sponsored: true, displayName: "", assets: [Asset](), relatedAssets: [RelatedAsset](), relatedImages: [Image](), assetType: "", timeStamp: 0)
            do {
                if let path = Bundle.main.path(forResource: "news", ofType: "json") {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let decoder = JSONDecoder()
                    let object = try decoder.decode(News.self, from: data)
                     seal.fulfill(object)
                }
                seal.fulfill(emptyNews)

            }catch {
          
               seal.fulfill(emptyNews)
            }
           
        }
    }
}

