//
//  ViewController.swift
//  newsapp
//
//  Created by Jin on 21/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//

import UIKit
import RxSwift
import Reachability

class NewsViewController: UIViewController {
    var tableView:UITableView = {
        let tableview = UITableView(frame: .zero, style: UITableView.Style.plain)
        return tableview
    }()
    private let scrollView = UIScrollView(frame: CGRect.zero)
    private var stackView = UIStackView()
    private var contentView = UIView(frame:.zero)
    private var refreshControl: UIRefreshControl?

    private var sectionHeader = [String]()
    private var displayNews = [Asset]()
    private let viewModel = NewsViewModel()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // observation, which is used to received the output result from viewmodel
        addObservation()
        // build the scrollable view of the section head view
        setupHeaderView()
        // build the news list
        setupTableView()
        //request API Server to get the news list
        loadDashBoard()
        // add pull to refresh feature
        addRefreshControl()
        title = "NEWS"
    }
    

    private func addRefreshControl() {
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl!.layer.zPosition = 1000
            refreshControl!.bounds = CGRect(x: refreshControl!.bounds.origin.x, y: 80, width: refreshControl!.bounds.size.width, height: refreshControl!.bounds.size.height)
            
            refreshControl!.addTarget(self, action: #selector(self.loadDashBoard), for: .valueChanged)
            refreshControl!.tintColor = UIColor.white
            tableView.refreshControl = refreshControl
        }
    }
    
    
    @objc private func loadDashBoard() {
        startLoading()
        // push load new api signal to viewmodel
        viewModel.inputs.refreshDashboard()
    }
    
    
    private func setupTableView(){
        tableView.layer.zPosition = 1001
        tableView.register(UINib(nibName: NewsItemTableViewCellString, bundle: Bundle.main), forCellReuseIdentifier: NewsItemTableViewCellString)
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = false
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource  = self
        tableView.backgroundColor = UIColor.gray
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    
    private func setupHeaderView() {
        // create header structure
        //scrollview
        //--contentview
        //---stackview
        //----UILabel-UILabel-UILabel-UILabel
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.showsHorizontalScrollIndicator = false
        let guide = view.safeAreaLayoutGuide
        scrollView.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0).isActive = true

        scrollView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        contentView = UIView(frame:.zero)
        contentView.backgroundColor = UIColor.clear
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)


        contentView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor).isActive = true
        contentView.heightAnchor.constraint(equalTo: scrollView.frameLayoutGuide.heightAnchor).isActive = true
        
        
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.spacing = 0
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true

        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
    }
    
    
    private func createTitle(title: String, index:Int) -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.tag = index
        
        let value = UILabel()
        value.translatesAutoresizingMaskIntoConstraints = false
        value.textColor = UIColor.black
        value.text = title
        value.font = UIFont.boldSystemFont(ofSize: 16)
        value.textAlignment = .center
        view.addSubview(value)
//
        value.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        value.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        value.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        value.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        view.addGestureRecognizer(tapGesture)
        return view
    }
    
    
    private func addObservation () {
        viewModel.outputs.updateDashboadHeadersSignal
            .subscribe(onNext: { [weak self] info in
                if let error = info.error {
                    self?.handleErrorView(error: error)
                    return
                }
                self?.sectionHeader = info.headers
                self?.displayNews = info.result
                DispatchQueue.main.async {
                    self?.stopLoading()
                    self?.reloadHeaderView()
                    self?.tableView.reloadData()
                    self?.refreshControl?.endRefreshing()
                }
        }).disposed(by: disposeBag)
        
        viewModel.outputs.updateNewsSignal
            .subscribe(onNext: { [weak self] info in
                self?.displayNews = info.result
                DispatchQueue.main.async {
                    self?.stopLoading()
                    self?.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
    }
    
    
    @objc private func tapAction(_ tap: UIGestureRecognizer) {
        if let tag = tap.view?.tag, tag < sectionHeader.count {
            self.startLoading()
            let key = sectionHeader[tag]
            self.viewModel.inputs.loadNewsBySection(key: key)
            print("tapped", tag)
        }
    }
    
    
    private func handleErrorView(error:Error) {
        var errorDescription = "Unknow Error"
        let errorTitle = "Error happening"
        if let localError = error as? LocalError {
            errorDescription = localError.localizedDescription
        }
        
        if let _ = error as? Reachability.ReachabilityStatus {
            errorDescription = "no internet connection, please try again"
        }
        if let errorDetail = error as? NSError {
            errorDescription = errorDetail.localizedDescription
        }
        
        let alertViewController = UIAlertController(title: errorTitle, message: errorDescription, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self] action in
            DispatchQueue.main.async {
                self?.refreshControl?.endRefreshing()
                self?.stopLoading()
            }
      
            self?.loadDashBoard()
        }))
        
        alertViewController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak self] action in
              DispatchQueue.main.async {
                  self?.refreshControl?.endRefreshing()
                  self?.stopLoading()
              }
          }))
        self.present(alertViewController, animated: true)
    }
    
    
    private func reloadHeaderView () {
        //refresh the section list of the head view
        for subview in stackView.arrangedSubviews {
           subview.removeFromSuperview()
           stackView.removeArrangedSubview(subview)
        }
        sectionHeader.enumerated().forEach { index, value in
            let view = createTitle(title: String(value).uppercased(), index: index)
            stackView.addArrangedSubview(view)
        }
        stackView.invalidateIntrinsicContentSize()
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
}

extension NewsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsItemTableViewCellString, for: indexPath) as! NewsItemTableViewCell
        cell.fillContentWith(asset: self.displayNews[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayNews.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

