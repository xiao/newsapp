//
//  NewsItemTableViewCell.swift
//  newsapp
//
//  Created by Jin on 23/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//

import UIKit
import Kingfisher

let NewsItemTableViewCellString = "NewsItemTableViewCell"
class NewsItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    @IBOutlet weak var byLineLabel: UILabel!
    @IBOutlet weak var relatedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func fillContentWith(asset: Asset?) {
        guard let asset = asset else {
            return
        }
        titleLabel.text = asset.headline
        abstractLabel.text = asset.theAbstract
        byLineLabel.text = asset.byLine
        guard let relatedImage = asset.relatedImages.first else {
            return
        }
        
        let url = URL(string: relatedImage.url)
        relatedImageView.contentMode = .scaleAspectFill
        let processor = DownsamplingImageProcessor(size: relatedImageView.frame.size)
                     |> RoundCornerImageProcessor(cornerRadius: 20)
        relatedImageView.kf.indicatorType = .activity
        relatedImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
