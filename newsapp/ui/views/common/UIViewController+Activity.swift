import Foundation
import NVActivityIndicatorView

// extension UIViewcontroller to conform NVActivityIndicatorViewable
// define the activity type
//
extension UIViewController: NVActivityIndicatorViewable {
    func startLoading() {
        self.startAnimating(type: NVActivityIndicatorType.lineScale)
    }
    
    func stopLoading() {
        self.stopAnimating()
    }
}
