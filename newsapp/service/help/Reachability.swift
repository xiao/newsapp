//
//  Reachability.swift
//  newsapp
//
//  Created by Jin on 22/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//

import Foundation
import PromiseKit
import Reachability

extension Reachability {
    public enum ReachabilityStatus: Error {
        case noInternetConnection
    }
    
    func isInternetReachable() -> Promise<Bool> {
        return Promise<Bool>() {seal in
            if connection != .unavailable {
                seal.fulfill(true)
            } else {
                seal.reject(ReachabilityStatus.noInternetConnection)
            }
        }
    }
}
