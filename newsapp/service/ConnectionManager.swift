import UIKit
import PromiseKit
import Reachability

enum LocalError: Error {
    case ServerError
    case ParseDataError
    case UnknownError
    
    var localizedDescription: String {
        switch self {
        case .ServerError:
            return "Faild to read data from server"
        case .ParseDataError:
            return "Faild to parse data to JSON"
        default:
            return "Unknown Error"
        }
    }
}

protocol ConnectionManagerProtocol {
    func getAllNews() -> Promise<News>
}

struct ConnectionManager: ConnectionManagerProtocol {
    let reachability = try! Reachability()
    func getAllNews() -> Promise<News> {
        return firstly {
            reachability.isInternetReachable()
        }.then { _ in
            return Promise { seal in
                let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
                let dataTask = defaultSession.dataTask(with: URL(string: UrlService.newslink)!) { data, response, error in
                      if let error = error {
                            //return the error
                           seal.reject(error)
                      }
                    guard let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200  else {
                           // server error
                          seal.reject(LocalError.ServerError)
                          return
                      }
                    do {
                        // return the parse News data
                        let decoder = JSONDecoder()
                        let object = try decoder.decode(News.self, from: data)
                        seal.fulfill(object)
                    } catch let parsingerror {
                        print("Error ", parsingerror)
                        // Failed to parse the json, return error
                        seal.reject(LocalError.ParseDataError)
                    }
                  }
                  dataTask.resume()
            }
        }
    }
}

struct UrlService {
    private static let NEWS_SERVICE = "https://bruce-v2-mob.fairfaxmedia.com.au/1/coding_test/13ZZQX/full"
    private static let NEWS_MOCK_SERVICE = "http://localhost:8080/1/coding_test/13ZZQX/full"
    
    
    static func isWiremock() -> Bool {
        if ProcessInfo.processInfo.arguments.contains("UI_TESTING") {
            print("using wiremock")
            return true
        } else {
            print("using real api")
            return false
        }
    }
    
    static var newslink: String {
        return UrlService.isWiremock() ? NEWS_MOCK_SERVICE : NEWS_SERVICE
    }
    
}



