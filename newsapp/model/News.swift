import Foundation
struct News: Codable {
    var id: Int
    var categories: [NewsCategory]
    var authors: [Author]
    var url: String
    var lastModified: Int
    var onTime: Int
    var sponsored: Bool
    var displayName: String
    var assets: [Asset]
    var relatedAssets: [RelatedAsset]
    var relatedImages: [Image]
    var assetType: String
    var timeStamp: Int
}


struct Asset: Codable {
    var id: Int
    var categories: [NewsCategory]
    var authors: [Author]
    var url: String
    var lastModified: Int
    var sponsored: Bool
    var headline: String
    var indexHeadline: String
    var tableHeadline: String?
    var theAbstract: String
    var body: String?
    var byLine: String
    var acceptComments: Bool
    var numberOfComments: Int
    var relatedAssets: [RelatedAsset]
    var relatedImages: [Image]
    var signPost: String?
    var companies: [Company]
    var legalStatus: String
    var liveArticleSummary: String?
    var sources: [Source]
    var relatedPosts: [Post]?
    var assetType: String
    var overrides: Overrides
    var extendedAbstract: String?
    var hasVideo: Bool?
    var timeStamp: Int
}


struct Image: Codable {
    var id: Int
    var categories: [NewsCategory]
    var brands: [Brand]
    var authors: [Author]
    var url: String
    var lastModified: Int
    var sponsored: Bool
    var description: String
    var photographer: String
    var type: String
    var width: Int
    var height: Int
    var assetType: String
    var xLarge2x: String?
    var xLarge: String?
    var large2x: String?
    var large: String?
    var thumbnail2x: String?
    var thumbnail: String?
    var timeStamp:Int
    
    enum CodingKeys: String, CodingKey {
        case id, categories, brands, authors, url, lastModified, sponsored, description, photographer, type, width, height, assetType, xLarge2x = "xLarge@2x", xLarge, large2x = "large@2x", large, thumbnail2x = "thumbnail@2x", thumbnail, timeStamp
    }
    
}

struct NewsCategory: Codable {
    var name: String
    var sectionPath: String
    var orderNum: Int
}

struct Author: Codable {
    var name: String
    var title: String
    var email: String
    var relatedAssets: [RelatedAsset]
    var relatedImages: [Image]
}

struct Company: Codable {
    var id: Int
    var companyCode: String
    var companyName: String
    var abbreviatedName: String
    var exchange: String
    var companyNumber: String?
}

struct Source: Codable {
    var tagId: String
}

struct Overrides: Codable {
    var overrideHeadline: String
    var overrideAbstract: String
}

struct Brand: Codable {
    
}

struct RelatedAsset: Codable {
    var id: Int
    var categories: [NewsCategory]
    var authors: [Author]
    var url: String
    var lastPublishedDate: Int?
    var lastModified: Int
    var onTime: Int?
    var sponsored: Bool
    var assetType: String
    var headline: String
    var timeStamp: Int
}

struct Post: Codable {
    var id: Int
    var categories: [NewsCategory]
    var authors: [Author]
    var lastModified: Int
    var sponsored: Bool
    var headline: String
    var body: String
    var authorNames: String
    var relatedAssets: [RelatedAsset]
    var relatedImages: [Image]
    var assetType: String
    var extendedAbstract: String
    var timeStamp: Int
}

