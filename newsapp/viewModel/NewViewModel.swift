//
//  NewViewModel.swift
//  newsapp
//
//  Created by Jin on 25/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//
import PromiseKit
import RxSwift
import RxCocoa
import Reachability

protocol NewsViewModelInputs {
    func refreshDashboard()
    func loadNewsBySection(key:String)
}

protocol NewsViewModelOutputs {
    var updateDashboadHeadersSignal: Observable<(result:[Asset], headers:[String], error:Error?)> { get }
    var updateNewsSignal:  Observable<(result:[Asset], error:Error?)> { get }
}

protocol NewsViewModelType {
    var inputs: NewsViewModelInputs { get }
    var outputs: NewsViewModelOutputs { get }
}

class NewsViewModel: NewsViewModelType, NewsViewModelInputs, NewsViewModelOutputs {
    init(scheduler: SchedulerType = MainScheduler.instance, server: ConnectionManagerProtocol = ConnectionManager()) {
        var displayNews = [Asset]()
        var sectionNews = [String:[Asset]]()
        var sectionHeader = [String]()

        // received the load news api signal
        let refreshDashboardSignal = refreshDashboardProperty.asObserver().share(replay: 1)
        self.updateDashboadHeadersSignal = refreshDashboardSignal
            .throttle(0.5, scheduler: scheduler)
            .flatMapLatest({ _ in
            return Observable.create({ observer in
                server.getAllNews().get { news in
                    displayNews = [Asset]()
                    sectionNews = [String:[Asset]]()
                    sectionHeader = [String]()
                    var assets = news.assets
                    assets.sort(by: {$0.timeStamp > $1.timeStamp})
                    //**** choose the totalnumber/4 news from the latest news as the top news
                    sectionNews.updateValue(([Asset]()), forKey: "top")
                    sectionHeader.append("top")
                     let num = assets.count/4
                    if num >= 1 {
                        sectionNews.updateValue(Array(assets[0..<num]), forKey: "top")
                    } else {
                        if let item = assets.first {
                            sectionNews.updateValue([item], forKey: "top")
                        }
                    }
                    
                    assets.forEach({ asset in
                        if let category = asset.categories.first {
                            //**** get the top level of sectionpath in category of each category
                            if let path = category.sectionPath.components(separatedBy: "/").filter({!$0.isEmpty}).first {
                                //**** if section exists,skip. otherwise add into the section list. which is used to shown in the section head view
                                if sectionHeader.firstIndex(of: path) == nil {
                                    sectionHeader.append(path)
                                }
                                // group the news
                                sectionNews = updateSectionsValue(value: asset, key: path, sectionNews: sectionNews)
                            } else {
                                // if unable to find the path. put the news into top to void mistake
                                sectionNews = updateSectionsValue(value: asset, key: "top", sectionNews: sectionNews)
                            }
                        }
                    })
                    // initial the page the top new
                    displayNews = sectionNews["top"] ?? [Asset]()
                    // push the result to view
                    observer.onNext((result:displayNews, headers:sectionHeader, error:nil))
                }.catch { error in
                    // error happening, push the error to view
                    observer.onNext((result:[], headers:[], error:error))
                }
                return Disposables.create()
            })
        })

        
        // received the load specific section news signal
        self.updateNewsSignal = loadSectionNewsProperty.asObserver().flatMapLatest({ key in
        return Observable.create({ observer in
                // find the news of that section
                displayNews = sectionNews[key] ?? [Asset]()
                // push the news to viewcontroller
                observer.onNext((displayNews, nil))
                return Disposables.create()
         })
        })
    }
    
    // receive the load news signal from viewcontrol
    let refreshDashboardProperty = PublishSubject<()>()
    func refreshDashboard() {
        refreshDashboardProperty.onNext(())
    }
    
    // receive the load specific section news from viewcontrol
    let loadSectionNewsProperty = PublishSubject<String>()
    func loadNewsBySection(key: String) {
        loadSectionNewsProperty.onNext(key)
    }
    
    var inputs: NewsViewModelInputs { return self }
    var outputs: NewsViewModelOutputs { return self }
    
    let updateDashboadHeadersSignal: Observable<(result:[Asset], headers:[String], error:Error?)>
    let updateNewsSignal:  Observable<(result:[Asset], error:Error?)>
}


fileprivate func updateSectionsValue(value:Asset, key:String, sectionNews:[String:[Asset]]) -> [String:[Asset]]  {
  var sectionNews = sectionNews
   if var assets = sectionNews[key] {
      assets.append(value)
      sectionNews.updateValue(assets, forKey: key)
   } else {
      sectionNews.updateValue([value], forKey: key)
   }
   return sectionNews
}
