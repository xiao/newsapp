//
//  newsappUITests.swift
//  newsappUITests
//
//  Created by Jin on 25/11/19.
//  Copyright © 2019 jinxiao. All rights reserved.
//
@testable import newsapp
import XCTest

class newsappUITests: XCTestCase {

    override func setUp() {
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
        var request = URLRequest(url: URL(string: "http://localhost:8080/__admin/scenarios/reset")!)
        request.httpMethod = "POST"
        defaultSession.dataTask(with: request)
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        app.launchArguments = ["UI_TESTING"];
        app.launchEnvironment = ["SnapshotTest": "true"];
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

     func testCellsNumberInTopSection() {
        let app = XCUIApplication()
        XCTAssert(app.tables.cells.count == 3)
        
        let elementsQuery = XCUIApplication().scrollViews.otherElements
        if elementsQuery.staticTexts["MARKETS"].exists {
            elementsQuery.staticTexts["MARKETS"].tap()
            XCTAssert(app.tables.cells.count == 1)
        }
        
        if elementsQuery.staticTexts["TOP"].exists {
            elementsQuery.staticTexts["TOP"].tap()
            XCTAssert(app.tables.cells.count == 3)
        }
        if elementsQuery.staticTexts["BUSINESS"].exists {
            elementsQuery.staticTexts["BUSINESS"].tap()
            XCTAssert(app.tables.cells.count == 6)
        }
        if elementsQuery.staticTexts["TECHNOLOGY"].exists {
            elementsQuery.staticTexts["TECHNOLOGY"].tap()
            XCTAssert(app.tables.cells.count == 3)
        }
        if elementsQuery.staticTexts["BRAND"].exists {
            elementsQuery.staticTexts["BRAND"].tap()
            XCTAssert(app.tables.cells.count == 1)
        }
        
    }
}
